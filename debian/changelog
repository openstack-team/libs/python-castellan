python-castellan (5.2.1-1) experimental; urgency=medium

  * New upstream release.
  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 15:28:57 +0100

python-castellan (5.1.1-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090472).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 11:39:50 +0100

python-castellan (5.1.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 16:41:44 +0200

python-castellan (5.1.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 10:27:09 +0200

python-castellan (5.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:05:08 +0200

python-castellan (5.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 05 Mar 2024 13:39:14 +0100

python-castellan (4.4.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Feb 2024 13:22:43 +0100

python-castellan (4.3.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 28 Sep 2023 17:02:50 +0200

python-castellan (4.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fix long description.

 -- Thomas Goirand <zigo@debian.org>  Sat, 23 Sep 2023 17:07:02 +0200

python-castellan (4.2.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Aug 2023 10:51:18 +0200

python-castellan (4.1.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1049009).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Aug 2023 13:54:29 +0200

python-castellan (4.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 10:25:03 +0200

python-castellan (4.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Feb 2023 14:22:09 +0100

python-castellan (4.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Sep 2022 22:06:20 +0200

python-castellan (4.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 17:15:14 +0200

python-castellan (3.10.2-1) unstable; urgency=medium

  * New upstream release.
  * Removed Replace_the_deprecated_argument_tenant.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 13 May 2022 11:11:19 +0200

python-castellan (3.10.1-3) unstable; urgency=medium

  * Add Replace_the_deprecated_argument_tenant.patch (Closes: #1009455).

 -- Thomas Goirand <zigo@debian.org>  Thu, 14 Apr 2022 12:00:38 +0200

python-castellan (3.10.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 12:04:19 +0100

python-castellan (3.10.1-1) experimental; urgency=medium

  * New upstream release.
  * Add python3-requests-mock to build-depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 18 Feb 2022 16:42:33 +0100

python-castellan (3.9.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 10:04:05 +0200

python-castellan (3.9.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 23 Aug 2021 16:19:25 +0200

python-castellan (3.7.1-2) unstable; urgency=medium

  * Upload to unstable

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 08:17:31 +0200

python-castellan (3.7.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed versions from (build-)depends when satisfied in Bullseye.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 09:59:21 +0100

python-castellan (3.6.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 22:44:08 +0200

python-castellan (3.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed min version of python3-cryptography.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 15:50:56 +0200

python-castellan (3.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-babel from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Sep 2020 09:15:12 +0200

python-castellan (3.0.2-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 15 Jul 2020 15:43:54 +0200

python-castellan (3.0.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 12:17:06 +0200

python-castellan (3.0.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 Apr 2020 22:48:24 +0200

python-castellan (3.0.0-1) experimental; urgency=medium

  * Fix upstream URL in d/rules, d/control and d/copyright.
  * New upstream release.
  * Add python3-sphinxcontrib.svg2pdfconverter as build-depends.
  * Add remove-privacy-breach-from-doc.patch.
  * Switch to debhelper-compat 11.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 16:52:06 +0200

python-castellan (1.3.1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 09:06:13 +0200

python-castellan (1.3.1-2) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Sep 2019 15:37:15 +0200

python-castellan (1.3.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2019 16:40:38 +0200

python-castellan (1.2.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 20:20:39 +0200

python-castellan (1.2.2-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Run unit tests from installed Python package.

 -- Thomas Goirand <zigo@debian.org>  Tue, 26 Mar 2019 17:50:55 +0100

python-castellan (0.19.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Jan 2019 23:14:43 +0100

python-castellan (0.18.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 04 Sep 2018 23:30:20 +0200

python-castellan (0.18.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Building sphinx doc with Python 3.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Aug 2018 15:54:50 +0200

python-castellan (0.17.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:28 +0000

python-castellan (0.17.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Running wrap-and-sort -bast

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Feb 2018 08:03:40 +0000

python-castellan (0.12.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 00:57:12 +0000

python-castellan (0.12.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Using pkgos-dh_auto_{install,test}.

 -- Thomas Goirand <zigo@debian.org>  Sun, 08 Oct 2017 23:07:03 +0200

python-castellan (0.4.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Mar 2016 17:47:43 +0100

python-castellan (0.3.1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Fixed debian/copyright ordering.

 -- Thomas Goirand <zigo@debian.org>  Fri, 22 Jan 2016 09:05:50 +0000

python-castellan (0.2.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 23:05:58 +0000

python-castellan (0.2.1-1) experimental; urgency=medium

  * Initial release. (Closes: #799622)

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Sep 2015 22:29:18 +0200
